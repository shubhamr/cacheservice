from flask import Flask
import sys
from flask_restful import reqparse, abort, Api, Resource
from CacheConnector import memcache_connector
from CacheLib import LRUCache

parser = reqparse.RequestParser()
parser.add_argument('value')

cache = None#, cache_store=memcache_connector.Client())

class CacheServer(Resource):
    
    def get(self, key):
        #print("Shubham", key, file=sys.stderr)
        #sys.stdout.flush()
        val = cache.get(key)
        if not val or val == -1:
            abort(404, message="key {} doesn't exist".format(key))
        return val
    
    def put(self, key):
        value = parser.parse_args()['value']
        #print("Shubham", key, value, file=sys.stderr)
        #sys.stdout.flush()
        return cache.set(key, value)

    #TODO implement delete here.
    def delete(self, key):
        cache.delete(key)
        return "", 200

if __name__ == '__main__':
    app = Flask(__name__)
    api = Api(app)
    capacity = 10
    try:
        capacity = int(sys.argv[1]) 
    except:
        print("Starting cache with a default capacity 10")
    cache = LRUCache(capacity=capacity)
    api.add_resource(CacheServer, '/cache/<key>')
    app.run(debug=True)
