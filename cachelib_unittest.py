import gc
import random
import sys
import unittest
from . import CacheLib 

SIZES = [1, 2, 10, 1000]
gettotalrefcount = getattr(sys, 'gettotalrefcount', lambda: 0)



class TestCacheLib(unittest.TestCase):

    def setUp(self):
        gc.collect()
        self._before_count = gettotalrefcount()

    def tearDown(self):
        after_count = gettotalrefcount()
        self.assertEqual(self._before_count, after_count)

    def test_lru1(self):
        
        l = CacheLib.LRUCache(1, {})
        l.set('a',1)
        l.get('a')
        print(list(l.d.keys()))
        self.assertEqual(list(l.d.keys()), ['a'])
        l.set('b', 2)
        self.assertEqual(list(l.d.keys()), ['b'])
        del l
        
    def test_lru2(self):
        l = CacheLib.LRUCache(2, {})
        print(l.d)
        l.set('a', 1)
        l.set('d', 2)
        print(l.d)
        self.assertEqual(len(l.d), 2)
        l.get('a')                  # Testing the first one
        l.set('c', 3)
        self.assertEqual(sorted(list(l.d.keys())), ['a', 'c'])
        l.get('c')
        self.assertEqual(sorted(list(l.d.keys())), ['a', 'c'])

    def test_lru3(self):
        l = CacheLib.LRUCache(3, {})
        l.set('a',1)
        l.set('b', 2)
        l.set('c', 3)
        self.assertEqual(len(l.d), 3)
        l.get('b')                  # Testing the middle one
        l.set('d', 4)
        self.assertEqual(sorted(list(l.d.keys())), ['b', 'c', 'd'])
        l.get('d')                  # Testing the last one
        self.assertEqual(sorted(list(l.d.keys())), ['b', 'c', 'd'])
        l.set('e', 5)
        self.assertEqual(sorted(list(l.d.keys())), ['b', 'd', 'e'])



if __name__ == '__main__':
    unittest.main()