"""
This file implements LRU cache module.
author: Shubham Rastogi
"""

class Node:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None
        self.prev = None
    def __str__(self):
        return str(self.key) + "_" + str(self.value)

    __repr__ = __str__

class LRUCache(object):

    # @param capacity, an integer, 
    # @param cache_store, dict based interface as object store. 
    def __init__(self, capacity, cache_store={}):
        self.capacity = capacity
        self.current_size = 0
        self.d = cache_store
        self.head = None
        self.end = None
        
        
    def get(self, key):
        node = self.get_node(key)
        #print node.value'
        if node:
            return node.value
        return -1
        
    def get_node(self, key):

        if key not in self.d:
            return None    
            
        node = self.d[key]
            
        if self.head != node:
            self.remove(node)
            self.set_head(node)
            
        #self.update_head(node)
        return node
        
    # @param key, an integer
    # @param value, an integer
    # @return nothing
    def set(self, key, value):
        self.__set(key, value) 
        #print "c", self.capacity, self.current_size, key, value
    
    def __set(self, key, value):
        #print("checking capacity", self.current_size, key, value)
        node = self.get_node(key)
        if node:
            node.value = value
            return
        node = Node(key, value)
        self.d[key] = node
        if self.head is None:
            self.head = node
            self.end = node
            self.current_size += 1
            return
        
        if self.capacity == self.current_size:
            #print("removing element capacity", self.current_size, key, value)
            del self.d[self.end.key]
            self.remove(self.end)
            self.current_size -= 1
        
        self.set_head(node)
        #print("setting head capacity", self.current_size, key, value)
        self.current_size += 1

    def delete(self, key):
        node = self.get_node(key)
        if not node:
            return
        del self.d[key]
        self.remove(node)
        self.current_size -= 1
        
        
    def set_head(self, node):
        
        if self.head:
            self.head.next = node
            node.prev = self.head
            node.next = None
        else :
            self.end = node
        self.head = node
        
        
        
    def remove(self, node):
        if node.prev:
            node.prev.next = node.next
        if node.next:
            node.next.prev = node.prev
            
        if not(node.prev or node.next):
            self.head = None
            self.end = None
        
        if self.end == node:
            self.end = node.next
            self.end.prev = None

"""
class WriteThroughCache(object):
    def __init__(self, store, size):
        self.store = store
        self.cache = LRUCache(size)

    def __get(self, key):
        val = self.cache.get(key)
        if val or val == -1:
            return val
        value = self.store[key]
        self.cache[key] = value
        return value

    def get(self, key, default=None):
        try:
            return self.__get(key)
        except KeyError:
            return default

    def set(self, key, value):
        self.cache.set(key, value)
        self.store[key] = value

    def delete(self, key):

        del self.store[key]


        try:
            self.cache.delete(key)
        except KeyError:
            pass
"""